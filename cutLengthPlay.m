function [soundCut] = cutLengthPlay(inputSound,newLenSound, namefile)


% THIS FUNCTION IS JUST TO CUT THE SOUNDS STIMULI for LePOULPE AT THE DESIRED LENGTH

% INPUTS: 
% 1) the sound
% 2) desired length in seconds 
% 3) name of the output file

% AUTHOR: giorgio marinato
% DATE: 15/07/2017


[data,Fs] = audioread(inputSound);
INFO = audioinfo(inputSound);

newLength = newLenSound*Fs

soundCut = data(1:newLength)

filename = [namefile, '.wav']

audiowrite(filename,soundCut,Fs)

end

