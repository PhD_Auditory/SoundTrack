

function [snd, sndRefined] = cutSounds(recordingPath, typeStimuli, subjSoundFolder, stimulusPath, stimSound, all_sounds, nsnds)

% This function does a bunch of things to cut as precise as possible the
% individual sound stimuli that can be used in other experiments. It takes
% time because the function saves a lot of figure that initially were used
% to develop the algorithm and now can be used as sanity check. The core
% algorithm start at "cut sounds".

% INPUTS:
% recordingPath
% typeStimuli
% subjSoundFolder
% rotation
% stimulusPath

% AUTHOR:       Giorgio Marinato @ CIMeC - UNiversity of Trento
% DATE:         15/05/2017
% LAST MOD:     04/08/2017
 
%%  ===== TO DO =====

% 1) cut the sounds -----> DONE
% 2) build a function with path to the recordings and path to the stimuli
% as inputs
% 2.2) save the figures in a folder in the subj folder -----> DONE
% 3) take the matrix of recorded sounds and cut precisely one second in the
% center -----> DONE
% 4) to fully automatize should build an algorithm to recongize the wanted
% length: if >2sec then cut 2sec etc


%%  ===== CLEAR CLOSE =====
% 
%     clear all
%     close all
%     

 %%  ===== SET THE BASICS =====
    
    figFolder       = '/Figures';
    
        if  ~exist([recordingPath, typeStimuli, subjSoundFolder])
            mkdir([recordingPath, typeStimuli, subjSoundFolder])
        end
% 
%     recordingPath   = '/home/giorgio/Projects/Auditory/SoundTrack/Data/IndividualRecordings/Federico_25072017';
%     typeStimuli     = '/white';
%     subjSoundFolder = ['/Federico_sound_stimuli'];
%     
%     figFolder       = '/Figures';
%     
%         if  ~exist([recordingPath, typeStimuli, subjSoundFolder])
%             mkdir([recordingPath, typeStimuli, subjSoundFolder])
%         end
%         
%     stimulusPath    = '/home/giorgio/Projects/Auditory/SoundTrack/soundsStimuli';   % path to the pink noise stimulus, or others
%     stimSound       = 'white_1s.wav'; 
%     sndsformat      = 'wav';
%     all_sounds      = dir(fullfile(recordingPath, typeStimuli, strcat('*.',sndsformat)));
%     nsnds           = length(all_sounds);
%     array_sound     = cell(nsnds,1);

%%  ===== READ FILES =====

    array_sound     = cell(nsnds,1);

    for s = 1:nsnds
        
        rotations       = audioread(fullfile(recordingPath,typeStimuli, all_sounds(s).name));
        info            = audioinfo(fullfile(recordingPath,typeStimuli, all_sounds(s).name));
        array_sound{s}  = rotations;
        sndsname{s}     = all_sounds(s).name;
    end

    Fs = info.SampleRate; 
    
    
%%  ===== AMPLIFY THE SOUNDS TO -1dB ===== 

    for amp = 1:nsnds
        
        array_snd_amp{amp} = amplifyFun(array_snd{amp});
        
    end        
    
%%  ===== EQUALIZE THE LENGTH OF THE SOUNDS  =====      

    minLen = min(cellfun(@(x) length(x), array_sound_amp))
        
    for r = 1:nsnds
    
        initDiscard = length(array_sound_amp{r,1})- minLen 
        
        if initDiscard == 0
                array_sound_eqLen{r} = array_sound_amp{r,1} 
                
        else        
            array_sound_eqLen{r} = array_sound_amp{r,1}(initDiscard:end,:)
        end
    end    
    
    array_sound_eqLen = transpose(array_sound_eqLen)
    
    
%% ===== VISUALLY INSPECT EACH ROTATION AUDIO FILE TO HAVE A GIST OF THE DATA =====
% TO DO : 
% 1) Subplot for left and right for each rotation ---> __DONE__
% from here display of figures are suppressed with: 

  set(0 ,'DefaultFigureVisible','off')



    for r = 1:nsnds
        
        figRotation = figure;
    
        subplot(2,1,1);
        plot(array_sound_eqLen{r,1}(:,1),'b');
        title('Left')
        xlabel('Samples')
        ylabel('Amplitude')
    
        subplot(2,1,2);
        plot(array_sound_eqLen{r,1}(:,2),'r');
        title('Right')
        xlabel('Samples')
        ylabel('Amplitude')

        % if you want save the figure for later inspection otherwise
        % comment out --maybe put in the figure folder
        
        saveas(figRotation, [recordingPath, typeStimuli, '/', typeStimuli, 'figRotations_', num2str(r)], 'svg');
        % close all
    end

    close all
    
                           
%%  ===== IDENTIFY THE MAX AMPLITUDE AND VARAINCE OF THE BACKGROUND NOISE  =====
% 
% % ===== take an initial piece with just noise()
% 
%      
%     cut_noise_from = 1; %seconds
%     cut_noise_till = 5; %seconds
% 
%     % here we use the last recording file that correspond to just noise and we use 
%     % "r", that now represent the last cycle of the previous for loop, in
%     % fact the noise recording
%     cut_noise = array_sound{r,1}(cut_noise_from*Fs:cut_noise_till*Fs); 
% 
%     figure
%     plot(cut_noise)
%     
%     close all
%     
% 
% % ===== calculate the max amplitude and the variance of the noise to find the threshold
% 
%     [maxval_Noise, maxloc_Noise] = max(abs(cut_noise));
% 
%     varVal = var(cut_noise);
%     
    
%%  ===== MAIN LOOP TO PROCESS ALL THE ROTATIONS: CUT THE SOUNDS IN EVERY FILE, WRITE THEM AS WAV, STORE THEM IN A CELL ARRAY, SAVE IT =====

    snd = cell(16,nsnds);

for nrot = 1:nsnds
%%  ===== MKDIR SOME FOLDERS TO SAVE FIGURES =====
    
    rot = ['/rot_', num2str(nrot)];
    
        if  ~exist([recordingPath, typeStimuli, subjSoundFolder, rot, figFolder]);
            mkdir([recordingPath, typeStimuli, subjSoundFolder, rot, figFolder]);
        end

        
%%  ===== ISOLATE MORE THE STIMULI PART =====


% ===== cut the first two seconds that are usually more noisey

    cut_initial_noise = 1; %seconds
    

% ===== diff (first derivate) to increase signal to noise ratio

    cut_stimuli_part = array_sound_eqLen{nrot,1}(cut_initial_noise*Fs:end,:);
    
    cut_stimuli_part_diff = diff(cut_stimuli_part);
    
      
% ===== figure object

    figClean = figure;
    
    p = uipanel('Parent',figClean,'BorderType','none'); 
    p.Title = 'A Cleaner Initial Noise'; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_stimuli_part_diff(:,1),'b');
        title('Left')
        xlabel('Samples')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_stimuli_part_diff(:,2),'r');
        title('Right')
        xlabel('Samples')
        ylabel('Amplitude')
        
        
% ===== save on hard disk

    saveas(figClean, [recordingPath, typeStimuli, subjSoundFolder, rot, figFolder, '/figClean_',num2str(nrot)], 'svg');
    
    close all
    
    
%%  ===== SET PARAMETERS: WHERE DO YOU START, DURATION OF WAV FILE, SILENCE ETC. ===== 
    
% ===== stimuli infos
    
    %pink        = audioread([stimulusPath, '/pink_1s.wav']);                         % audioread(to the path of the stimulus)
    stimSoundInfo    = audioinfo([stimulusPath, '/', stimSound]);                         % audioinfo(to the path of the stimulus)
    
    
% ===== isolate 2s. of clean noise at the beginnig of the rotation file    
    
    cut_noise_from = 0.5; %seconds
    cut_noise_till = 2.5; %seconds
    
    cut_noise_rot = cut_stimuli_part_diff(cut_noise_from*Fs:cut_noise_till*Fs,:);
    
    
% ===== visually inspect the starting noise   
    
    figStartingNoise = figure
    
    p = uipanel('Parent',figStartingNoise,'BorderType','none');
    p.Title = ['Starting Noise for Threshold for the Rotation N. ', num2str(nrot)]; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_noise_rot(:,1),'b');
        title('Left')
        xlabel('Samples')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_noise_rot(:,2),'r');
        title('Right')
        xlabel('Samples')
        ylabel('Amplitude')
        
        
% ===== save on hard disk
        
     saveas(figStartingNoise, [recordingPath, typeStimuli, subjSoundFolder, rot, figFolder, '/figStartingNoise_', num2str(nrot)], 'svg');
     
     close all
     
        
% ===== calculate the max amplitude of the starting noise of this rotation to find the threshold   

    [maxval_Rot, maxloc_Rot] = max(abs(cut_noise_rot(:)));
    
    
% ===== reassign new length more near to the first sound to increase the
% likelihood of detection of the real start & diff

    cut_stimuli_part = cut_stimuli_part(cut_noise_till*Fs:end,:)
    
    cut_stimuli_part_diff = diff(cut_stimuli_part);
    
    
% ===== build the treshold and set all the other parameters to cut the sounds

    initSafeAmp     = 0.1;

    threshold   = maxval_Rot + initSafeAmp;
    stimLength  = stimSoundInfo.TotalSamples;       % duration in samples
    gap         = 0.50*Fs;                          % samples
    delta       = gap/2;                            % samples
    
    
    init_cut = find(abs(cut_stimuli_part_diff)>threshold,1,'first');
        
    cut_stimuli_part_first = cut_stimuli_part_diff(init_cut:end,:);
    
    
% ===== visually inspect the starting point aka init_cut    
    
    figStartingPoint = figure;
    
    p = uipanel('Parent',figStartingPoint,'BorderType','none'); 
    p.Title = ['Starting Point for the Current Rotation N. ', num2str(nrot)]; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_stimuli_part_first(:,1),'b');
        title('Left')
        xlabel('Samples')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_stimuli_part_first(:,2),'r');
        title('Right')
        xlabel('Samples')
        ylabel('Amplitude')
        
    
% ===== save on hard disk

    saveas(figStartingPoint, [recordingPath, typeStimuli, subjSoundFolder, rot, figFolder, '/figStartingPoint_',num2str(nrot)], 'svg');
    
    close all

%% ===== CUT THE SOUNDS =====       

    for chunkSound = 1:16 
        
        
        
        if chunkSound == 1
        
            startSG            = init_cut -(delta/4);
            stopSG             = init_cut+stimLength+delta;
            SG                 = cut_stimuli_part(startSG:stopSG,:);
            
            [startStimulus, ~] = find(abs(diff(SG))>threshold,1,'first');
            %startStimulus      = init_cut 
            [stopStimulus, ~]  = find(abs(diff(SG))>threshold,1,'last');
              
            snd{chunkSound,nrot(:)}            = SG(startStimulus:stopStimulus,:);
            
            init_cut = stopSG + delta
            
% ===== save figures of broad cut on hard disk to later inspection
            
            figSG = figure
            
            p = uipanel('Parent',figSG,'BorderType','none'); 
            p.Title = ['broad cut for Rotation N.', num2str(nrot), ' chunk N.', num2str(chunkSound)]; 
            p.TitlePosition = 'centertop'; 
            p.FontSize = 12;
            p.FontWeight = 'bold';
        
                subplot(2,1,1,'Parent',p);
                plot(SG(:,1),'b');
                title('Left')
                xlabel('Samples')
                ylabel('Amplitude')
    
                subplot(2,1,2,'Parent',p);
                plot(SG(:,2),'r');
                title('Right')
                xlabel('Samples')
                ylabel('Amplitude')
            
           
            saveas(figSG, [recordingPath, typeStimuli, subjSoundFolder, rot, figFolder, '/figSG_',num2str(nrot), '_', num2str(chunkSound)], 'svg');
            
            close all            
            
% ===== let's continue with the algorithm            
        
        else
            
            startSG            = init_cut-delta;
            stopSG             = init_cut+stimLength+delta;
            SG                 = cut_stimuli_part(startSG:stopSG,:);
            
            localSafeAmp = 0.001
            localsafeLen = 2000
            
            set_local_threshold_First                   = cut_stimuli_part_diff((startSG:init_cut)-localsafeLen,:)   
            [maxval_Local_First, maxloc_Local_First]    = max(abs(set_local_threshold_First(:)))
            threshold_local_First                       = maxval_Local_First + localSafeAmp
            
            set_local_threshold_Last                    = cut_stimuli_part_diff((stopSG-delta)+localsafeLen:stopSG,:)   
            [maxval_Local_Last, maxloc_Local_Last]      = max(abs(set_local_threshold_Last(:)))
            threshold_local_Last                        = maxval_Local_Last + localSafeAmp
            
            [startStimulus, ~] = find(abs(diff(SG))>threshold_local_First,1,'first');
            [stopStimulus, ~]  = find(abs(diff(SG))>threshold_local_Last,1,'last');
        
            snd{chunkSound,nrot(:)}            = SG(startStimulus:stopStimulus,:);
        
            init_cut = stopSG + delta
            
% ===== save figures of broad cut on hard disk to later inspection
            
            figSG = figure
            
            p = uipanel('Parent',figSG,'BorderType','none'); 
            p.Title = ['broad cut for Rotation N.', num2str(nrot), ' chunk N.', num2str(chunkSound)]; 
            p.TitlePosition = 'centertop'; 
            p.FontSize = 12;
            p.FontWeight = 'bold';
        
                subplot(2,1,1,'Parent',p);
                plot(SG(:,1),'b');
                title('Left')
                xlabel('Samples')
                ylabel('Amplitude')
    
                subplot(2,1,2,'Parent',p);
                plot(SG(:,2),'r');
                title('Right')
                xlabel('Samples')
                ylabel('Amplitude')
            
           
            saveas(figSG, [recordingPath, typeStimuli, subjSoundFolder, rot, figFolder, '/figSG_',num2str(nrot), '_', num2str(chunkSound)], 'svg');
            
            close all
            
                    
            %audiowrite([recordingPath, subjSoundFolder, rot, '/wav_', num2str(nrot), num2str(rs), '.wav'], snd{chunkSound,nrot}(:,:), Fs);
        end
    end
    
    
 %% ===== PLOT THE CUT SOUND FOR THE CURRENT ROTATION AND SAVE ON HARD DRIVE =====     

    for chunkSound = 1:16
    
        figCut = figure;
    
        subplot(2,1,1);
        plot(snd{chunkSound,nrot}(:,1),'b');
        title('Left')
        xlabel('Samples')
        ylabel('Amplitude')
    
        subplot(2,1,2);
        plot(snd{chunkSound,nrot}(:,2),'r');
        title('Right')
        xlabel('Samples')
        ylabel('Amplitude')
        
        saveas(figCut, [recordingPath, typeStimuli, subjSoundFolder, rot, '/figCut_', num2str(chunkSound)], 'svg');
    
    end
     
    close all
    
end

%% ===== CUT A PRECISE 1S (=44100 SAMPLES) OF SOUND IN THE CENTER OF EACH =====

    sndRefined = cell(16,nsnds);

    for idx = 1:numel(snd)
        
        lenIdx = length(snd{idx}); 
        chop   = 1*Fs        
        remain = round(lenIdx-chop)/2;        
        
        sndRefined{idx} = snd{idx}(remain:(chop+remain)-1,:);                
        
    end
    
end
