function [wav, wav_sub]=Giorgio_concat_sound_stimuli(recordingPath)

wav = [];

for k=1:35
    [wav_sub, FS] = audioread([recordingPath, num2str(k), '.wav']);

end

audiowrite([recordingPath, '/Giorgio_sound_stimuli/traj_', num2str(1), '.wav'], wav, FS);
