function [wav_sub] = Giorgio_cut_sound_stimuli(recordingPath, stimLength, subjSoundFolder, rotation)

% ===== INPUTS =====
% subjSoundFolder
% rotation

[wav, Fs] = audioread([recordingPath, '/STE-000.wav']);
samples = 1:length(wav);
wav_d = abs(diff(wav(:,1))); 
wav_id = wav_d>=0.03;

samples_on = samples(wav_id==1); %in only ~8000 samples there is a signal

samples_start = [];
for i=samples_on
    if wav_id(i-5000:i-1)==0 %find those signals where there's a 1000-sample pause before
        samples_start = [samples_start, i];
    end
end

samples_start = samples_start(samples_start>2*Fs); %discard noise at the beginning of recording session

%dur = FS/1000* 200; %each tone 200ms long?
dur = stimLength*Fs;

k=0;

if ~exist([recordingPath, subjSoundFolder])
   mkdir([recordingPath, subjSoundFolder])
end

if ~exist([recordingPath, subjSoundFolder, rotation])
   mkdir([recordingPath, subjSoundFolder, rotation])
end

for i=samples_start
    k=k+1
    wav_sub = wav(i:i+dur, :); 
    audiowrite([recordingPath, '/Greg_sound_stimuli/', rotation, '/wav_', num2str(k), '.wav'], wav_sub, Fs);
end
