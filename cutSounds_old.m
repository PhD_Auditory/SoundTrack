%%  ===== TO DO =====

% function [snd] = cut_sounds(recordingPath,stimulusPath,subjSoundFolder,rot) 
% inputs:
% recordingPath
% subjSoundFolder
% rotation
% stimulusPath
 

% 1) cut the sounds --> DONE
% 2) build a function with path to the recordings and path to the stimuli
% as inputs
% 2.2) save the figures in a folder in the subj folder --> DONE




%%  ===== CLEAR CLOSE =====

    clear all
    close all
    

%%  ===== SET THE BASICS =====

    recordingPath   = '/home/giorgio/Projects/Auditory/SoundTrack/Data/IndividualRecordings/Greg_1sPn_10deg/'
    subjSoundFolder = 'Greg_sound_stimuli'
    
    figFolder       = '/Figures'
    
        if  ~exist([recordingPath, subjSoundFolder])
            mkdir([recordingPath, subjSoundFolder])
        end
        
    stimulusPath    = '/home/giorgio/Projects/Auditory/SoundTrack/soundsStimuli'   % path to the pink noise stimulus, or others
    sndsformat      = 'wav';
    all_sounds      = dir(fullfile(recordingPath,strcat('*.',sndsformat)));
    nsnds           = length(all_sounds);
    array_sound     = cell(nsnds,1);

%%  ===== READ FILES =====

    for s = 1:nsnds
        
        rotations       = audioread(fullfile(recordingPath,all_sounds(s).name));
        info            = audioinfo(fullfile(recordingPath,all_sounds(s).name));
        array_sound{s}  = rotations;
        sndsname{s}     = all_sounds(s).name;
    end

%% ===== VISUALLY INSPECT EACH ROTATION AUDIO FILE TO HAVE A GIST OF THE DATA =====
% TO DO : 
% 1) Subplot for left and right for each rotation ---> __DONE__
% 2) break the loop every rotation and continue when you close the figure
% --> NOT USEFUL



    for r = 1:nsnds
        
        figRotation = figure;
    
        subplot(2,1,1);
        plot(array_sound{r,1}(:,1),'b');
        title('Left')
        xlabel('Time')
        ylabel('Amplitude')
    
        subplot(2,1,2);
        plot(array_sound{r,1}(:,2),'r');
        title('Right')
        xlabel('Time')
        ylabel('Amplitude')

        % if you want save the figure for later inspection otherwise
        % comment out
        
        saveas(figRotation, [recordingPath, 'figRotations_', num2str(r)], 'svg');
        % close all
    end

%%  ===== IDENTIFY THE MAX AMPLITUDE AND VARAINCE OF THE BACKGROUND NOISE ===== 

% ===== take an initial piece with just noise

    Fs = info.SampleRate;

    cut_noise_from = 1; %seconds
    cut_noise_till = 5; %seconds

    % here we use the last recording file that correspond to just noise and we use 
    % "r", that now represent the last cycle of the previous for loop, in
    % fact the noise recording
    cut_noise = array_sound{r,1}(cut_noise_from*Fs:cut_noise_till*Fs); 

    figure
    plot(cut_noise)
    

% ===== calculate the max amplitude and the variance of the noise to find the threshold

    [maxval_Noise, maxloc_Noise] = max(abs(cut_noise));

    varVal = var(cut_noise);
    
    
%%  ===== PROCESS ALL THE ROTATIONs: CUT THE SOUNDS IN EVERY FILE, WRITE THEM AS WAV, STORE THEM IN A CELL ARRAY, SAVE IT =====

    snd = cell(16,35);

for nrot = 1:35 
%%  ===== MKDIR SOME FOLDERS TO SAVE FIGURES =====
    
    rot = ['/rot_', num2str(nrot)]
    figFolder = '/Figures'
    
        if  ~exist([recordingPath, subjSoundFolder, rot, figFolder])
            mkdir([recordingPath, subjSoundFolder, rot, figFolder])
        end

        
%%  ===== ISOLATE MORE THE STIMULI PART =====

% ===== cut the first two seconds that are usually more noisey

    cut_initial_noise = 2.5; %seconds
    

% ===== diff (first derivate) to increase signal to noise ratio

    cut_stimuli_part = diff(array_sound{nrot,1}(cut_initial_noise*Fs:end,:));
    
    
% ===== figure object

    figClean = figure;
    
    p = uipanel('Parent',figClean,'BorderType','none'); 
    p.Title = 'A Cleaner Initial Noise'; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_stimuli_part(:,1),'b');
        title('Left')
        xlabel('Time')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_stimuli_part(:,2),'r');
        title('Right')
        xlabel('Time')
        ylabel('Amplitude')
        
        
% ===== save on hard disk

    saveas(figClean, [recordingPath, subjSoundFolder, rot, figFolder, '/figClean_',num2str(nrot)], 'svg');
    
    
%%  ===== SET PARAMETERS: WHERE DO YOU START, DURATION OF WAV FILE, SILENCE ETC. ===== 
    
% ===== stimuli infos
    
    pink        = audioread([stimulusPath, '/pink_1s.wav']);                         % audioread(to the path of the stimulus)
    pinkInfo    = audioinfo([stimulusPath, '/pink_1s.wav']);                         % audioinfo(to the path of the stimulus)
    
    
% ===== isolate 1s. of clean noise at the beginnig of the rotation file    
    
    cut_noise_from = 0.2; %seconds
    cut_noise_till = 1.5; %seconds
    
    cut_noise_rot = diff(cut_stimuli_part(cut_noise_from*Fs:cut_noise_till*Fs,:));
    
    
% ===== visually inspect the starting noise   
    
    figStartingNoise = figure
    
    p = uipanel('Parent',figStartingNoise,'BorderType','none');
    p.Title = ['Starting Noise for Threshold for the Current Rotation N. ', num2str(nrot)]; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_noise_rot(:,1),'b');
        title('Left')
        xlabel('Time')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_noise_rot(:,2),'r');
        title('Right')
        xlabel('Time')
        ylabel('Amplitude')
        
        
% ===== save on hard disk
        
     saveas(figStartingNoise, [recordingPath, subjSoundFolder, rot, figFolder, '/figStartingNoise_',num2str(nrot)], 'svg');
     
        
% ===== calculate the max amplitude of the starting noise of this rotation to find the threshold   

    [maxval_Rot, maxloc_Rot] = max(abs(cut_noise_rot(:)));
    

% ===== build the treshold and set all the other parameters to cut the sounds

    safeAmp     = 0.0010;
%     if maxval_Rot
%     end
    threshold   = maxval_Rot + safeAmp;
    stimLength  = pinkInfo.TotalSamples;             % duration in samples
    gap         = 0.50*Fs;                          % samples
    delta       = gap/2;                             % samples
    
    init_cut = find(abs(cut_stimuli_part)>threshold,1,'first');
        
    cut_stimuli_part_first = cut_stimuli_part(init_cut:end,:);
    
    
% ===== visually inspect the starting point aka init_cut    
    
    figStartingPoint = figure;
    
    p = uipanel('Parent',figStartingPoint,'BorderType','none'); 
    p.Title = ['Starting Point for the Current Rotation N. ', num2str(nrot)]; 
    p.TitlePosition = 'centertop'; 
    p.FontSize = 12;
    p.FontWeight = 'bold';
        
        subplot(2,1,1,'Parent',p);
        plot(cut_stimuli_part_first(:,1),'b');
        title('Left')
        xlabel('Time')
        ylabel('Amplitude')
    
        subplot(2,1,2,'Parent',p);
        plot(cut_stimuli_part_first(:,2),'r');
        title('Right')
        xlabel('Time')
        ylabel('Amplitude')
        
    
% ===== save on hard disk

    saveas(figStartingPoint, [recordingPath, subjSoundFolder, rot, figFolder, '/figStartingPoint_',num2str(nrot)], 'svg');
    

%% ===== CUT THE SOUNDS =====


    for chunkSound = 1:16 
        
        if chunkSound == 1
        
            startSG            = init_cut-delta;
            stopSG             = init_cut+stimLength+delta;
            SG                 = cut_stimuli_part(startSG:stopSG,:);
            [startStimulus, ~] = find(abs(SG)>threshold,1,'first');
            [stopStimulus, ~]  = find(abs(SG)>threshold,1,'last');
              
            snd{chunkSound,nrot(:)}            = SG(startStimulus:stopStimulus,:);
            
            init_cut = stopSG + delta
            
        
        else
            startSG            = init_cut-delta;
            stopSG             = init_cut+stimLength+delta;
            SG                 = cut_stimuli_part(startSG:stopSG,:);
            
                       
            set_local_threshold = cut_stimuli_part(startSG:init_cut,:)   
            
            [maxval_Local, maxloc_Local] = max(abs(set_local_threshold(:)));
            
            threshold_local = maxval_Local + safeAmp
            
            [startStimulus, ~] = find(abs(SG)>threshold_local,1,'first');
            [stopStimulus, ~]  = find(abs(SG)>threshold_local,1,'last');
        
            snd{chunkSound,nrot(:)}            = SG(startStimulus:stopStimulus,:);
        
            init_cut = stopSG + delta
            
            
        
            %audiowrite([recordingPath, subjSoundFolder, rot, '/wav_', num2str(nrot), num2str(rs), '.wav'], snd{chunkSound,nrot}(:,:), Fs);
        end
    end
    
    
 %% ===== PLOT THE CUT SOUND FOR THE CURRENT ROTATION =====     
% 
%     for chunkSound = 1:16
%     
%         figCut = figure;
%     
%         subplot(2,1,1);
%         plot(snd{chunkSound,nrot}(:,1),'b');
%         title('Left')
%         xlabel('Time')
%         ylabel('Amplitude')
%     
%         subplot(2,1,2);
%         plot(snd{chunkSound,nrot}(:,2),'r');
%         title('Right')
%         xlabel('Time')
%         ylabel('Amplitude')
%         
%         %save
%         % saveas(figRotation, [recordingPath, subjSoundFolder, rot, '/figCut_', num2str(chunkSound)], 'svg');
%     
%     end
    
end
%% ===== SAVE ON HARD THE ORDERED ARRAY =====

% ===== save on hard disk
%
%     the variable snd, it's a cell array: the columns
%     correspond to rotations, and the rows corresponds to the vertical sounds of
%     each rotations

    save([recordingPath, subjSoundFolder, '/sndArray.mat'], 'snd', '-v7.3');
