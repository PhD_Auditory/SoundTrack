%%
clear all 
close all

%%  ===== SET THE BASICS =====
% I used the same names of the input that I put in the function because
% they're self explanatory. 

    recordingPath   = '/home/giorgio/Projects/Auditory/SoundTrack/Data/IndividualRecordings/Francesco_26072017';
    typeStimuli     = '/white';
    subjSoundFolder = ['/Francesco_26072017_sound_stimuli_2'];
    
    stimulusPath    = '/home/giorgio/Projects/Auditory/SoundTrack/soundsStimuli';   % path to the pink noise stimulus, or others
    stimSound       = 'white_1s.wav'; 
    sndsformat      = 'wav';
    all_sounds      = dir(fullfile(recordingPath, typeStimuli, strcat('*.',sndsformat)));
    nsnds           = length(all_sounds);
    

    [snd_FrancescoWhite, sndRefined_FrancescoWhite] = cutSounds(recordingPath, typeStimuli, subjSoundFolder, stimulusPath, stimSound, all_sounds, nsnds)
    
    
%% ===== SAVE ON HARD DRIVE THE ORDERED ARRAY =====

% ===== save on hard disk

%     the variable sndRefined, it's a cell array: the columns
%     correspond to rotations, and the rows corresponds to the vertical sounds of
%     each rotations

    save([recordingPath, typeStimuli, subjSoundFolder, '/sndRefined_FrancescoWhite.mat'], 'sndRefined_FrancescoWhite', '-v7.3');
