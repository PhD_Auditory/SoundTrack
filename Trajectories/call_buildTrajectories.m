%% ===== CALL SCRIPT TO BUILD_TRAJECTORIES =====

% this sript has to be used to call the function build trajectories and
% save the trajectories on disc

% AUTHOR: giorgio marinato @ CIMEC -University of Trento
% DATE: 22/08/2017


%% ===== DRAW AS MANY RANDOM TRAJECTORIES AS NEEDED =====

% ______SELECT THE INDIVIDUAL SOUND MATRIX

Fs = 44100

sndMatrixPath = '/home/giorgio/Projects/Auditory/SoundTrack/Data/IndividualRecordings/Federico_25072017/white/Federico_sound_stimuli'

trajFolder = '/Trajectories'

    if  ~exist([sndMatrixPath, trajFolder])
        mkdir([sndMatrixPath, trajFolder])
    end

snd_matrix_name = '/sndArray.mat'

load([sndMatrixPath, snd_matrix_name])
whos

%% ______CALL THE FUNCTION IN THE LOOP AS MANY TRAJECTORIES ARE NEEDED

trajStruct = struct()

for ntraj = 1:50
    
    [trajVector] = build_trajectories_GM(sndRefined, 50);
    
    trajStruct(ntraj).trajVector = trajVector
           
% ______EXTRAPOLATE THE CORRESPONDING SOUNDS USING THE VECTOR

    trajSound = sndRefined(trajVector)
    
    trajStruct(ntraj).trajSound = trajSound    
% ______CONCATENATE THE SOUNDS

    traj = []
    
    trajSound = transpose(trajSound)
    
    traj = cell2mat(trajSound)
    
    trajStruct(ntraj).traj = traj
    
end

%% ===== SAVE AND WRITE TO DISK THE STRUCTURE AND THE SINGLE FILES =====

save([sndMatrixPath, '/FedericoWhiteTraj.mat'], 'trajStruct', '-v7.3')

for i = 1: length(trajStruct)

 audiowrite([sndMatrixPath, trajFolder, '/traj_', num2str(i), '.wav'], trajStruct(i).traj, Fs);
end



