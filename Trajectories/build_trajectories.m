%% ===== WE'RE GOING TO BUILD A FUNCTION TO RANDOMLY SKETCH SOUND TRAJECTORIES ===== 

% THE SOUND MATRIX SHOULD BE LOADED IN THE WORKSPACE

% TRAJECTORIES NOT IN USE SHOULD BE COMMENTED OUT, 
% VARIABLES ARE SHARED

%% ===== GENERATE RANDOM CELL ARRAY OF NORMALLY DISTRIBUTED RANDOM NUMBERS REPRESENTING OUR SOUNDS =====

sound                       = randn(44100,2);
snd                         = cell(16,35);
snd(:)                      = {sound};

% B                           = 0;
% num_grid                    = zeros(16,35);
% 
% for i=1:560
%     B                       = B+1;
%     num_grid(i)             = B;
% end

% %% ===== FIRST TRY TO DESIGN A NOT RANDOM TRAJECTORY WITH A INDEXING VECTOR =====
% 
% % _________EQUATOR RUN_____________________________________
% 
% [rowSnd, colSnd]            = size(snd);    % rowSnd=16,colSnd=35
% start                       = snd{8,1};     % center speaker, origin
% stop                        = snd{:,colSnd};
% 
% traj_1_vector               = snd(8:16:552);   % simple lap around the equator
% traj_vert                   = vertcat(traj_1_vector{:});
% 
% soundsc(traj_vert, 44100);
% 
% % _________PONG____________________________________________
%  
% positions                   = [];
% vert_max_el                 = 1:16:560;
% vert_min_el                 = 16:16:560;
% up                          = 15;
% down                        = 17;
% spk_pos                     = 8;
% current_pos                 = snd(spk_pos);
% traj_2_vector{1,1}          = snd(8);
% 
% goin_up                     = true;
% 
% for i = 1:length(snd)-1
%     if goin_up == true
%         spk_pos             = spk_pos+up;
%         positions(i)        = spk_pos;
%         POS(i).val          = up;
%         hi_bound            = ismember(positions,vert_max_el);
%         if hi_bound(i)==1
%             goin_up         = false;
%         end
%     else
%         spk_pos             = spk_pos+down;
%         positions(i)        = spk_pos;
%         POS(i).val          = down;
%         lo_bound            = ismember(positions,vert_min_el);
%         if lo_bound(i)==1
%             goin_up         = true; 
%         end
%     end
%     traj_2_vector{i+1,1}    = current_pos;
% end
% 
% traj_2                      = vertcat(traj_2_vector{:});
% pong                        = vertcat(traj_2{:});
% soundsc(pong, 44100);
% http://www.crushsite.it/it/danza-teatro/2017/sogni-doro-la-favola-vera-di-adriano-olivetti-festival-contavalle.html
% 
% % % _________OVERHEAD ARCH __________________________________
% % 
% % % This one starts at the center speaker, then moves along the 
% % % horizontal plane to the right until just past 90 degrees,(110
% % % in this case). Once there, the sound will begin to travel up 
% % % and behind the participant, ascending until it encounters the
% % % upper limit at 180 degrees, as close as we can get to right above
% % % them, at which point it will precede down until it hits a
% % % given limit (250 degrees), here provided by the 25th column or 'arm' of the 
% % % Poulpe. From there it will continue back to the origin on a 
% % % level trajectory. 
% % 
% positions                   = [];
% vert_max_el                 = 1:16:560;
% vert_min_el                 = 16:16:560;
% horz_bound_r                = num_grid(:,11);
% horz_bound_l                = num_grid(:,25);
% up                          = 15;
% level                       = 16;
% down                        = 17;
% spk_pos                     = 8;
% current_pos                 = snd(spk_pos);
% traj_3_vector{1,1}          = snd(8);
% 
% lvl                         = true;
% goin_up                     = false;
% 
% for i = 1:length(snd)-1
%     if lvl == true
%         spk_pos             = spk_pos+level;
%         positions(i)        = spk_pos;
%         POS(i).val          = level;
%         horz_r              = ismember(positions,horz_bound_r);
%         if horz_r(i)==1
%             lvl             = false;
%             goin_up         = true;
%         end
%     elseif goin_up == true
%         spk_pos             = spk_pos+up;
%         positions(i)        = spk_pos;
%         POS(i).val          = up;
%         hi_bound            = ismember(positions,vert_max_el);
%         if hi_bound(i)==1
%             goin_up         = false;
%         end
%     else
%         spk_pos             = spk_pos+down;
%         positions(i)        = spk_pos;
%         POS(i).val          = down;
%         horz_l              = ismember(positions,horz_bound_l);
%         if horz_l(i)==1
%             lvl             = true; 
%         end
%     end
%     traj_3_vector{i+1,1}    = current_pos;
% end
% 
% 
% traj_3                      = vertcat(traj_3_vector{:});
% overhead                    = vertcat(traj_3{:});
% soundsc(overhead, 44100);
% 
% %% ===== PLOTTING 2D TRAJECTORIES =====
% 
% b = 8;
% for i=1:length(positions)
%     if POS(i).val == 15
%         b = b+1;
%     elseif POS(i).val == 17
%         b = b-1;
%     else
%         b = b;
%     end
%     for_plot(i)=b;
% end
% 
% plot(for_plot);

%% ====== RANDOMIZED w/ RIGHTWARD MOTION ONLY =====

% positions                   = [];
% vert_max_el                 = 1:16:560;
% vert_min_el                 = 16:16:560;
% up                          = 15;
% level                       = 16;
% down                        = 17;
% 
% possible_directions         = [up, level, down]; % only allows movement to the right (up, down, level)
% msize                       = numel(possible_directions);
% chance_mat                  = possible_directions(randperm(msize, 3));
% 
% senza_up                    = [level, down];
% susize                      = numel(senza_up);
% chance_senza_up             = senza_up(randperm(susize, 2));
% 
% senza_down                  = [level, up];
% sdsize                      = numel(senza_down);
% chance_senza_down           = senza_down(randperm(sdsize, 2));
% 
% spk_pos                     = 8;
% current_pos                 = snd(spk_pos);
% traj_4_vector{1,1}          = snd(8);
% 
% chance                      = true;
% goin_up                     = false;
% goin_down                   = false;
% 
% for i = 1:length(snd)-1
%     chance_mat              = possible_directions(randperm(msize, 3));
%     if chance == true
%         spk_pos             = spk_pos+chance_mat(1);
%         positions(i)        = spk_pos;
%         hi_bound            = ismember(positions,vert_max_el);
%         lo_bound            = ismember(positions,vert_min_el);
%         if hi_bound(i)==1
%             chance          = false;
%         elseif lo_bound(i)==1
%             chance          = false;
%             goin_up         = true;
%         end
%     elseif goin_up == false
%         spk_pos             = spk_pos+chance_senza_up(1);
%         positions(i)        = spk_pos;
%         hi_bound            = ismember(positions,vert_max_el);
%         if hi_bound(i)==0
%             chance          = true;
%         end
%     else
%         spk_pos             = spk_pos+chance_senza_down(1);
%         positions(i)        = spk_pos;
%         lo_bound            = ismember(positions,vert_min_el);
%         if lo_bound(i)==0
%             chance          = true;
%             goin_up         = false;
%         end
%     end
%     traj_4_vector{i+1,1}    = current_pos;
% end
%  
% traj_4                      = vertcat(traj_4_vector{:});
% rand_traj                   = vertcat(traj_4{:});
% % soundsc(rand_traj, 44100);
% 
% % _______ PLOTTING _______________________________________
% 
% b = 8;
% for i=1:length(positions)
%     if i == 1
%         if positions(i)-b == 15
%             b = b+1;
%         elseif positions(i)-b == 17
%             b = b-1;
%         else
%             b = b;
%         end
%     elseif positions(i)-positions(i-1) == 15
%         b = b+1;
%     elseif positions(i)-positions(i-1) == 17
%         b = b-1;
%     else
%         b = b;
%     end
%     for_plot(i)=b;
% end
% 
% graph = plot(for_plot);
% graph;
% 
% set(graph,'linewidth',1);
% 
% h               = gca;
% h.FontSize      = 10;
% h.XLim          = [0 35];
% h.YLim          = [0 16];
% h.XGrid         = 'on';
% h.YGrid         = 'on';
% title('Random Trajectory');
% xlabel('Degrees of Rotation (10�)');
% ylabel('Vertical Speaker Position');
% yticks([0:1:16]);
% xticks([0:1:35]);
% xticklabels({'0�', '', '','','','','','','','90�','','','','','','','','','180�','','','','','','','','','270�','','','','','','','','350�'});


%% ===== PICK A RANDOM START IN THE MATRIX ===== FROM GIORGIO CODE

% ===== PICK A RANDOM START IN THE MATRIX

rndm_start                      = randi(numel(snd));%returns number of element in matrix
current_pos                     = rndm_start;
[current_idx_R, current_idx_C]  = ind2sub(size(snd), current_pos);%returns indices
positions{1}                    = [current_idx_R, current_idx_C];%logs starting point

% ===== TRY (1) GENERATE TWO VECTORS OF RANDOM RELATIVES POSITIONS FOR xy INDECES 
% this is not that elegnat for the boundaries

XrandomIntegers             = randi([-1,1],[1,1000]);
YrandomIntegers             = randi([-1,1],[1,1000]);

coordinates(1,:)            = XrandomIntegers;
coordinates(2,:)            = YrandomIntegers;

bad_columns                 = find(coordinates(1,:)==0 & coordinates(2,:)==0);

% These were another way of creating boundaries, but accessing the
% different indices of 'next()' seems to do the trick just fine
%
% too_high                    = find(coordinates(1,:)==-1);
% too_low                     = find(coordinates(1,:)==1);
% too_left                    = find(coordinates(2,:)==-1);
% too_right                   = find(coordinates(2,:)==1);

% some kind of conditional deletion or suppression of inappropriate
% coordinates
% e.g. - all navigational instructions that include -1 in the row
% position are essentially saying "reduce your row position by 1", so we
% eliminate all those from coordinate space when we detect the upper boundary. The
% next question is how to make sure they're reintroduced or simply not
% completely omitted so that they will be available when normal navigation
% can resume.

coordinates(:,bad_columns)  = [];
vert_max_el                 = 1;
[vert_min_el, horz_bound_r] = size(snd);
horz_bound_l                = 1;
lenPath                     = 50;
tornare                     = isequal(positions{path},positions{path-2});
tornare_new                 = isequal((next(path).*-1),next(path-1));%trying out with indices

% I think tornare_new will return the coordinate path that you took to get
% to your current position. So ideally you could see if the last set of
% navigational instructions, when multiplied elementwise by -1, are equal
% to the previous set of instructions, because that would mean that you're
% actually are going back where you came from. 

% not sure if the rule should be utilizing the positions{} or the next()
% coordinates, which would involve arithmetic to compare. 

% right now the biggest problem is that I'm trying to make sure that a new
% position does not equal the position two iterations ago (path-2), which
% means returning to the 'previous' position. Problem is, obviously 'path'
% is less than 2 for 2 iterations, so I'm having a hard time defining a
% variable (tornare, above ^^) outside of the for loop ,as well as for the
% first couple loops. 

for path = 1:lenPath
    if current_idx_R~=(vert_max_el) && current_idx_R~=(vert_min_el) && current_idx_C~=(horz_bound_r) && current_idx_C~=(horz_bound_l)
        pick_next                       = randi([1,length(coordinates)],[1,1]);%changed to +1
        next                            = coordinates(:,pick_next);
        if tornare == 1
            return
        else
            curr                            = transpose([current_idx_R, current_idx_C]);
            current_pos                     = curr+next;
            current_idx_R                   = current_pos(1);
            current_idx_C                   = current_pos(2);
            clear curr
        end
    elseif current_idx_R == vert_max_el
        pick_next                       = randi([-1,length(coordinates)],[1,1]);
        next                            = coordinates(:,pick_next);
        if next(1) == -1 || tornare == 1
            return
        else
            curr                            = transpose([current_idx_R, current_idx_C]);
            current_pos                     = curr+next;
            current_idx_R                   = current_pos(1);
            current_idx_C                   = current_pos(2);
            clear curr
        end
    elseif current_idx_R == vert_min_el
        pick_next                       = randi([-1,length(coordinates)],[1,1]);
        next                            = coordinates(:,pick_next);
        if next(1) == 1 || tornare == 1
            return
        else
            curr                            = transpose([current_idx_R, current_idx_C]);
            current_pos                     = curr+next;
            current_idx_R                   = current_pos(1);
            current_idx_C                   = current_pos(2);
            clear curr
        end
    elseif current_idx_C == horz_bound_l
        pick_next                       = randi([-1,length(coordinates)],[1,1]);
        next                            = coordinates(:,pick_next);
        if next(2) == -1 || tornare == 1
            return
        else
            curr                            = transpose([current_idx_R, current_idx_C]);
            current_pos                     = curr+next;
            current_idx_R                   = current_pos(1);
            current_idx_C                   = current_pos(2);
            clear curr
        end
    else
        pick_next                       = randi([-1,length(coordinates)],[1,1]);
        next                            = coordinates(:,pick_next);
        if next(2) == 1 || tornare == 1
            return
        else
            curr                            = transpose([current_idx_R, current_idx_C]);
            current_pos                     = curr+next;
            current_idx_R                   = current_pos(1);
            current_idx_C                   = current_pos(2);
            clear curr
        end
    end     
    positions{path+1}                   = [current_idx_R, current_idx_C];
end


% _____PLOT ATTEMPT__________________________


% 
%     rndm_new_pos = current_pos() 
%     spk_pos = current_pos+rndm_new_pos
% 
% [I,J] = ind2sub(size(snd), positions)
%  plot(J,I)
 

 % ===== TRY (2) WALK STEP BY STEP STILL WITH THE -1 0 +1 METHOD
% possible positions are up(1,0) down(-1,0) diagup(1,1) diagdown(-1,1)
% right(0,1)
% add exception for boundaries probably make a while loop

