%%

clear all;


sound_dir = 

%initialize the sound driver
InitializePsychSound(0);
Fs = 44100;
numChannels = 2;

ntrials = 10; % # of trials to repeat 24 positions

%Defining the directory
Dir = pwd;
Dir_log = [pwd filesep 'logfiles'];
all_sound = repmat(sound_dir,[1, ntrials]);
Numsounds = length(all_sound);
rndNum_p = Shuffle(1:Numsounds); %% PSEUDORANDOM IS MISSING!



%Get the Participant ID

snum = input('Participant number?:','s');
sub =sprintf('Subj%s',snum);

trialnum = str2double(input('\nThe trial number:','s'));
RT = sprintf('%s%s%s_PositionPlus_%d.txt',Dir_log,sep,sub,trialnum);
logfile=fopen(RT,'w');
fprintf(logfile,'Trial#\t Seconds\t  User_response\t Correct_response\t Correctness\t PlayedSound\n');
savefileP = sprintf('%s%s%s_PositionPlus_%d.mat',Dir_log,sep,sub,trialnum);




%Position Experiment
pahandle = PsychPortAudio('Open',[],[],0,Fs,numChannels);

%CENTER SOUND
pahandle2 = PsychPortAudio('Open',[],[],0,Fs,numChannels);
filename2 = sprintf('%s%s%s_center.wav',sub,sep,sub);
fullname2 = fullfile(Dir, filename2);
[CenterData,Fs]=audioread(fullname2);
CenterData = CenterData';
PsychPortAudio('FillBuffer',pahandle2,CenterData);    
    
for i = 1:Numsounds
    keyIsDown =0;
    chosen_dir{i} = all_sound{rndNum_p(i)};
    chosen_dirName = chosen_dir{i};
    
    
    filename = sprintf('%s%s%s_%s.wav',sub,sep,sub,chosen_dirName);
    fullname = fullfile(Dir, filename);
    [SoundData,Fs]=audioread(fullname);
    SoundData = SoundData';
    %Play the sound & GetSecs
    
    PsychPortAudio('FillBuffer',pahandle,SoundData);
    
    WaitSecs(0.1); % gap for after responding
    PsychPortAudio('Start', pahandle2, [],[],1);
    WaitSecs(.260);
    playsound = PsychPortAudio('Start', pahandle, [],[],1);
    WaitSecs(waittime);
    startTime = GetSecs;
    
    
end     
PsychPortAudio('Close',pahandle);
PsychPortAudio('Close',pahandle2);
