function [traj] = Concat_sound_stimuli(sndArray)

% THIS FUNCTION CREATE A DUMB TRAJECTORY JUST CONCATENATING THE SOUNDS
% AUTHOR:       Giorgio Marinato
% DATE:         27/05/2017
% LAST CHANGE: 


traj = [];

for tr = 1:16
    
    traj = [traj; sndArray{tr,1}]
    
end

audiowrite([recordingPath, subjSoundFolder, '/traj_', num2str(1), '.wav'], traj, Fs);
