function [positions] = build_trajectories_GM(snd_matrix, lenPath)
% this function draws a trajectory of sound from the individual sound matrix that can be obtained from the cutSound function 

% INPUTS: 
% snd_matrix --> the individual sound matrix outcoming from cutSounds func.
% lenPath --> numbers of total steps of the trajectory 

% AUTHOR:       Giorgio Marinato & Gregory Ginnan @ CIMeC - UNiversity of Trento
% DATE:         12/08/2017
% LAST MOD:     21/08/2017



%% ===== RANDOM TRAJECTORIES ===== 

% _______PICK A RANDOM START IN THE MATRIX

rndm_start                      = randi(numel(snd_matrix)); %returns number of element in matrix
current_pos                     = rndm_start;
[current_idx_R, current_idx_C]  = ind2sub(size(snd_matrix),current_pos);


% _______DEFINE BASIC POSITION AND BOUNDARIES

positions                       = [];

vert_max_elev                   = 1
[vert_min_elev, horz_bound_r]   = size(snd_matrix);
horz_bound_l                    = 1;
%lenPath                         = 50;
pick                            = randi([0,1], [2,1])

%% ===== START THE LOOP TO GENERATE THE TRAJECTORY =====

for path = 1:lenPath  
    
    while current_idx_R~=(vert_max_elev) && current_idx_R~=(vert_min_elev) && current_idx_C~=(horz_bound_r) && current_idx_C~=(horz_bound_l)
          
% _______AVOID FALLING IN THE PREVIOUS POSITION
% this is working even when and if we want to allow going "leftwards" 
                
        pool                                = [-1, 0, 1]
        
        C_next                              = randi([0,1], [1,1])
        
        if pick(1) ~= 0
            
            pool(pool==(-pick(1)))          = []
            
            if C_next == 0                
                pool(pool==0)               = []
                R_next                      = pool(:,(randi(length(pool),[1,1])))
            else
                R_next                      = randi([pool(1),pool(length(pool))], [1,1])
            end
            
        else%if pick_next(1) == 0
            R_next                          = randi([pool(1),pool(length(pool))], [1,1])
            
            if R_next == 0
                C_next                      = 1
            end
        end
        
        pick                                = [R_next;C_next]

        break
    end

% _______AVOID GETTING STUCK IN THE UPPER LIMIT

    if current_idx_R == (vert_max_elev)
        
        pool                                = [ 0, 1]
        
        if pick(2) == 1
            
            C_next                          = randi([0,1], [1,1])
            
            if C_next == 1
                % _______SPECIAL CASE CORNERS
                if current_idx_C == (horz_bound_r)
                    C_next                  = -(size(snd_matrix,2)-1)
                end
                % _______END SPECIAL CASE CORNERS
               R_next                       = randi([0,1], [1,1])
            elseif C_next == 0
               R_next                       = 1 
            end
            
        elseif pick(2) == 0
            % _______SPECIAL CASE CORNERS
            if current_idx_C == (horz_bound_r)
                C_next                      = -(size(snd_matrix,2)-1)
            end
            % _______END SPECIAL CASE CORNERS
            
            C_next                      = 1
            R_next                      = randi([0,1], [1,1])
        
        end
        
        pick                                = [R_next;C_next]
        
    end       

% _______AVOID GETTING STUCK IN THE LOWER LIMIT
    
    if current_idx_R == (vert_min_elev)
        
        pool                                = [-1, 0,]
        
        if pick(2) == 1
            
            C_next                          = randi([0,1], [1,1])
            
            if C_next == 1
                % _______SPECIAL CASE CORNERS
                if current_idx_C == (horz_bound_r)
                    C_next                  = -(size(snd_matrix,2)-1)
                end
                % _______END SPECIAL CASE CORNERS
               R_next                       = pool(:,randi(length(pool), [1,1]))
            elseif C_next == 0
               R_next                       = -1 
            end
            
        elseif pick(2) == 0
            % _______SPECIAL CASE CORNERS
            if current_idx_C == (horz_bound_r)
                C_next                      = -(size(snd_matrix,2)-1)
            end
            % _______END SPECIAL CASE CORNERS    
                                
            C_next                      = 1
            R_next                      = pool(:,randi(length(pool), [1,1]))
                        
        end
                            
        pick                                = [R_next;C_next]
        
    end
  
% _______REFRAME THE RIGHT LIMIT TO START FROM THE BEGINNING OF THE MATRIX

    if current_idx_C == (horz_bound_r)
        
        pool                                = [-1, 0, 1]
        
        C_next                              = randi([0,1], [1,1])
        
        pool(pool==(-pick(1)))              =  []
        R_next                              = randi([pool(1),pool(length(pool))], [1,1])
        
        if C_next == 1
            C_next                          = -(size(snd_matrix,2)-1)
        end
        
        pick                                = [R_next;C_next]
    end
    
% _______REFRAME THE LEFT LIMIT TO START FROM THE END OF THE MATRIX
% this will be necessary if and when we want to build trajecotries that can go
% backwards

% _______DO THE ACTUAL MOVE

    curr                            = transpose([current_idx_R, current_idx_C]);
    current_pos                     = curr+pick; %should log actual trajectory
    current_idx_R                   = current_pos(1)
    current_idx_C                   = current_pos(2)
    
    if pick(2) == -(size(snd_matrix,2)-1)
        pick(2)                = current_idx_C
    end
        
    positions(path)                 = sub2ind(size(snd_matrix),current_pos(1),current_pos(2));

end

end
