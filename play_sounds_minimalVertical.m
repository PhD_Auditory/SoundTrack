

%%%%%%%%%%%%%%%
%%% INPUTS %%%
% stimuli_path = path where sound stimuli are located
%
% START the AO
% Slot2 ao0-14 is left arm
% Slot2 ao15-30 is right arm (center speaker included here), ao30 is center
% Slot3 ao0-14 upper arm
% Slot3 ao15-29 below arm
% all 4 arms, the direction is approaching to the center

%%%% TO DO: 
% build a function out of this script
% Check delay and gap --> DONE!
%% ===== CLEAR ALL =====
    
clear all;
close all;

WaitSecs(0.1)
tic

    
%% ===== SET THE OUTPUT,SAMPLE RATE, AND CHANNELS OF ANALOG OUT =====

AOLR=analogoutput('nidaq','PXI1Slot3');
out_AO=daqhwinfo(AOLR);
set(AOLR, 'SampleRate', 44100);
addchannel(AOLR,0:30);

% AnalogLeftRight safe db range
out_ranges=get(AOLR.Channel,'OutputRange');
setverify(AOLR.Channel,'OutputRange', [-5 5]);
setverify(AOLR.Channel,'UnitsRange', [-5 5]);
set(AOLR,'TriggerType', 'Manual');


%% ===== SET THE DIGITAL IN-OUT =====
% (FOR LED I THINK) 

value = 1;                                        % 0 1 (change value of central led: hor or vert sound bar)
dio = digitalio('nidaq', 'PXI1Slot3');
addline (dio, 0:7, 'out');
putvalue(dio.Line(1),value);


%% ===== SOUND FILES =====

%stimuli_path = addpath(genpath(stimuli_path));                   % the project folder in which you have sounds stimuli Giorgio=.../SoundTrack/sounds 
%pathname = '/fem_voice';                         % path to the folder of the single sound
pathname = '/media/giorgio/SMILE/SoundTrack/soundsStimuli/'

files_sound = {};                                 % empty array for the names of each sound (one for each speaker)
array_sound={};                                   % empty array for the actual sounds 

nspeaker = 31;

for i = 1:nspeaker 
        files_sound{i} = fullfile(pathname,strcat('whiteprova','.wav'));    % read in names 
        [array_sound{i}, ~] = audioread(files_sound{i});                            % read in actual WAV sounds
end


amp = 1;                                          % the intensity of sound

% calculate the gap between the single sounds 
gap = .50;                                   % half a second
AOLR.SampleRate = 44100;                          % sample rate of analogue output
gap = gap * AOLR.SampleRate;            % gap between sounds in samples

% build arrays to point the wanted sound to the wanted speaker
array_speaker = [1:15 31:-1:16]                   % one direction (from up to down) --> this gives the physical order and position of the speakers on LePoulpe
chosen_sound = repmat(1:nspeaker,[1 1]);          % in this case the "repmat" is redundant. Just change values if/when you want to create various apparent motions
                                               
seq_CH = [array_speaker; chosen_sound];           % in this case first row for the speakers, second row for the sounds/audio


wav_length=0;
for ch=1:size(seq_CH,2)                           % here is equal to the number of the speakers, but if you want to do apparent motion can be double or more
    wav_length= length(array_sound{seq_CH(2,ch)});% length of the sounds
end

% build the actual matrix that reflect the sequence in time of sounds in
% the corresponding speaker
data=[];
data= zeros(wav_length,nspeaker);                 % matrix to allocate space: row for samples equal to length of sounds, column for the number of the speaker.  
iniz=0;                                           % for the next for loop counter  
fin=0;
%length(array_speaker)

for j = 1:5
    
    ch = array_speaker(j);
    so = chosen_sound(j);
            
    iniz= fin+1
    fin=iniz+length(array_sound{chosen_sound(j)})-1+ gap % here there's the real import of the sound corresponding to the particular speaker (+gap)
    data(iniz:(fin-gap),array_speaker(j))=amp*array_sound{chosen_sound(j)}   % Here we position the sounds in the data matrix to reflect the sequence of speaker-sounds *2 looks like amplifier here??
end

figure;imagesc(data)
dur = size(data,1)/44100; %in sec

%% START
putdata(AOLR,data) % to queue the obj
% Start AO, issue a manual trigger, and wait for
% the device object to stop running.
start(AOLR)
%pause(1) %when to start exp
trigger(AOLR)
%stop(AO) terminates the execution

wait(AOLR, dur+1) %wait before doing anything else

delete(dio)
clear dio

delete(AOLR)
clear AO

WaitSecs(0.75)

toc


