sndPath = '/home/giorgio/Projects/Auditory/OverlappingScene/sounds/foreground/Russian.wav';

snd = audioread(sndPath);
sndinfo= audioinfo(sndPath);

Fs = sndinfo.SampleRate
nBits= sndinfo.BitsPerSample

infoDev = audiodevinfo

getfield(audiodevinfo, 'input');

dev_id = getfield(getfield(audiodevinfo, 'input'), 'ID');

recObj = audiorecorder (44100,24,2);

record(recObj);
recordblocking(recObj, 60);

soundsc(snd,Fs,nBits);

stop(recObj);

play(recObj)
y = getaudiodata(recObj);
plot(y)
