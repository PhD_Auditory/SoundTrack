% [snds,nsnds,sndsname] = readSounds(pathname,sndsformat)
%
% Loads all sounds of the type specified by sndsformat that are saved in 
% the folder specified by pathname
%
% INPUT:
% (1) pathname: directory (e.g. pathname = '/Applications/MATLAB/work')
% (2) sndsformat: file format of the input sounds (e.g., sndsformat = 'wav')
%
% OUTPUT:
% (1) snds: a cell containing the sounds matrices
% (2) nsnds: number of sounds

% ------------------------------------------------------------------------
% ------------------------------------------------------------------------

function [snds,nsnds,sndsname] = readSounds(pathname,sndsformat,outputpathname)

all_sounds = dir(fullfile(pathname,strcat('*.',sndsformat)));
nsnds = length(all_sounds);
snds = cell(nsnds,1);

if nargout == 3
sndsname = cell(nsnds,1);
end

for snd = 1:nsnds
    snd1 = wavread(fullfile(pathname,all_sounds(snd).name));
    info = wavfinfo(fullfile(pathname,all_sounds(snd).name));
    snds{snd} = snd1;
    sndsname{snd} = all_sounds(snd).name;
end
    
  
end

