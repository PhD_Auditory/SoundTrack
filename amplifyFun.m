function [ soundAmp ] = amplifyFun( inputSound )
%AMPLIFYFUN Amplify the sounds read with the audioread function to -1dB
%   The data are already normalized by the audioread function between the
%   values of -1 and 1. 1.0 = 0 dBFS. Thus I'm going to make the loudness 
%   of all the files playing at loudness of -1 dB

% AUTHOR:       Giorgio Marinato @ CIMeC - UNiversity of Trento
% DATE:         08/09/2017
% LAST MOD:     08/09/2017
       
        soundAmp = inputSound*.891/max(abs(inputSound(:)))

end

